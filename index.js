// Type "Hello World" then press enter.
var robot = require("robotjs");
const clip = require('clipboardy');
const words = ["undefined issue", "null pointer exception", 
"lint js", "js in aspx", "script tag type missing", "save in cache", "https on localhost", 
"fast array chack", "parse array fast", "invert array", "reverse items in array", "sort not working linq", "linq sort by name"]
StartBot();

const largeString = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.`;

function StartBot() {
    ShiftApp();
    SearchInGoogle(function () {
        setTimeout(() => {
            ShiftTab();
            setTimeout(() => {
                ShiftApp();
                setTimeout(() => {
                    ShiftTab();
                    robot.keyTap("enter");
                    KeyTabRepeat("down", 100, function(){
                        StartBot()
                    });
                }, 5000);
            }, 2000);
        }, 3000);
    });
}

function CloseTab(){
    robot.keyTap("w", ["command"]);
}

function ShiftApp() {
    robot.keyTap("tab", ["command"]);
    robot.keyTap("enter");
}

function ShiftTab() {
    robot.keyTap("tab", ["control"]);
}

function SearchInGoogle(callback) {
    CloseTab();
    var len = words.length - 1;

    setTimeout(function () {
        robot.keyTap("t", ["command"])
    }, 1000);
    setTimeout(function () {
        //clip.writeSync('love'); robot.keyTap("v", ["command"]);

        var rnd = Math.floor((Math.random() * len) + 1);
        var word = words[rnd];
        TypeString(word);

        // Press enter.
        robot.keyTap("enter");

        setTimeout(function () {
            KeyTabRepeat("down", 30, function () {
                TypeString(largeString);
                robot.moveMouse(500,500);
                robot.mouseClick()
                setTimeout(() => {
                    callback();                    
                }, 5000);
            });
        }, 2000);
    }, 1000);
}

function KeyTabRepeat(key, times, callback) {
    for (var i = 0; i < times; i++) {
        robot.keyTap(key);
    }
    callback();
}

function TypeString(str) {
    console.log(str);
    for (var i = 0; i < str.length; i++) {
        robot.keyTap(str[i]);
    }
}

function WaveMouse() {
    // Speed up the mouse.
    robot.setMouseDelay(2);

    var twoPI = Math.PI * 2.0;
    var screenSize = robot.getScreenSize();
    var height = (screenSize.height / 2) - 10;
    var width = screenSize.width;

    for (var x = 0; x < width; x++) {
        y = height * Math.sin((twoPI * x) / width) + height;
        robot.moveMouse(x, y);
    }

}